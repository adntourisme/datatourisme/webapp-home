/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var gulp = require('gulp');

/**
 * Variables
 */
var entries = [
    'submodules/webapp-bundle/Resources/assets/**/*',
    'app/Resources/assets/**/*'
];

/**
 * @type {{build: module.exports.build, watch: module.exports.watch}}
 */
module.exports = {
    build: function() {
        return gulp.src(entries)
            .pipe(gulp.dest('web/assets'));
    }
};
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
var jQuery = require("jquery");

(function ( $ ) {
    'use strict';

    var createHovers = function(hoveredClassName, highlghtedClassName) {

        var $hoveredClassName = $(hoveredClassName);
        var $highlghtedClassName = $(highlghtedClassName);

        $hoveredClassName.hover( function() {
            $highlghtedClassName.addClass('hover');
        }, function() {
            $highlghtedClassName.removeClass('hover');
        });
    }

    $(function yourFunction() {

        var $producer = createHovers('.cta--producer', '.banner--producer, .hero--producer');
        var $diffuser = createHovers('.cta--diffuser', '.banner--diffuser, .hero--diffuser');
        var $discover = createHovers('.cta--discover', '.banner--discover');

    });

}( jQuery ));

<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Symfony\Component\Translation\Loader\PoFileLoader;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new SessionServiceProvider());

// twig
$app->register(new TwigServiceProvider(), array(
    'twig.path' => array(__DIR__.'/../app/Resources/views'),
    'twig.options' => array('cache' => __DIR__.'/../var/cache/twig')
));
$app['twig.loader.filesystem'] = $app->extend('twig.loader.filesystem', function ($loader, $app) {
    $loader->addPath(__DIR__.'/../submodules/webapp-bundle/Resources/views', 'DatatourismeWebApp');
    return $loader;
});

// locale
$app->register(new LocaleServiceProvider(), array(
    'locale' => 'fr'
));
$app->register(new TranslationServiceProvider(), array(
    'locale_fallbacks' => array('fr'),
));

$app->extend('translator', function($translator, $app) {
    $translator->addLoader('po', new PoFileLoader());
    $translator->addResource('po', __DIR__.'/../submodules/webapp-bundle/Resources/translations/messages.en.po', 'en');
    $translator->addResource('po', __DIR__.'/../submodules/webapp-bundle/Resources/translations/messages.fr.po', 'fr');
    return $translator;
});

// globals
$app['request'] = $app->factory(function ($app) {
    return $app['request_stack']->getCurrentRequest();
});

return $app;

<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array(
        'status_code' => $code,
        'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : ''
    )), $code);
});

// redirect /fr to /
$app->get('/fr', function () use ($app) {
    return $app->redirect($app["url_generator"]->generate("homepage"));
});

// home
$app->get('/{_locale}', function () use ($app) {
    return $app['twig']->render('index.html.twig', array());
})
->value('_locale', 'fr')
->assert('_locale', '\w{2}')
->bind('homepage');

// terms of use
$app->get('/{_locale}/legal', function () use ($app) {
    $tpl = 'legal/legal.'.$app['locale'].'.html.twig';
    if(!$app['twig']->getLoader()->exists($tpl)) {
        $tpl = 'legal/legal.html.twig';
    }
    return $app['twig']->render($tpl, array());
})->bind('legal');

// legacy
$app->get('/mentions-legales', function () use ($app) {
    return $app->redirect($app["url_generator"]->generate("legal"));
});